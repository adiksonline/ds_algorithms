package com.data.struct;



public class RBTree {
	private Node rootNode;
	
	public RBTree(){
	}
	
	public Node getRoot(){
		return rootNode;
	}
	
	public void setRoot(Node node){
		node.setRed(false);
		rootNode = node;
		rootNode.setRed(false);
	}
	
	public void updateRoot(){
		Node root = rootNode;
		while (root.getParent() != null)
			root = root.getParent();
		rootNode = root;
	}
	
	public void insert(Integer key){
		Node node = new Node(key);
		if (rootNode == null){
			setRoot(node);
		} else {
			rootNode.append(node);
			updateRoot();
		}
		
	}
	
	public Node find(Integer key){
		Node node = rootNode;
		while (node.getValue().compareTo(key) != 0 || node.isDeleted()){
			if (node.getValue().compareTo(key) > 0)
				node = node.getLeft();
			else
				node = node.getRight();
			if (node == null)
				break;
		}
		return node;
	}
	
	public boolean delete(Integer key){
		Node node = find(key);
		if (node == null){
			return false;
		} else {
			node.setDeleted(true);
			return true;
		}
	}
	
	public void displayTree(){
		int space = 64;
		Stack<Node> globalStack = new Stack<Node>(1000);
		Stack<Node> localStack = new Stack<Node>(1000);
		globalStack.push(rootNode);
		boolean doNext = true;
		while (doNext ==true){
			doNext = false;
			for (int i = 0; i < (space / 2); i++)
				System.out.print(" ");
			while (! globalStack.isEmpty()){
				Node item = globalStack.pop();
				if (item == null){
					localStack.push(null);
					localStack.push(null);
					System.out.printf("%-3s", "*");
				} else {
					if ((item.getLeft() != null) || (item.getRight() != null))
						doNext = true;
					localStack.push(item.getLeft());
					localStack.push(item.getRight());
					char c = (item.isRed()) ? 'R' : 'B';
					System.out.printf("%-2s%s", item.getValue(), c);
				}
				for (int i = 0; i < space - 3; i++)
					System.out.print(" ");
			}
			while (! localStack.isEmpty())
				globalStack.push(localStack.pop());
			System.out.println();
			space /= 2;
		}
		System.out.println();
		
	}
	
	public static void main(String[] args){
		//long a = Runtime.getRuntime().availableProcessors();
		//System.out.println(a);
		RBTree rb = new RBTree();
		for (int i = 15; i >= 1; i--){
			int n = (int) (Math.random() * 100);
			rb.insert(n);
		}
		/*int[] n = {50, 25, 75, 12, 37, 31, 43, 28};
		for (int i : n)
			rb.insert(i);*/
		System.out.println(rb.delete(37));
		System.out.println(rb.find(37));
		System.out.println(rb.delete(37));
		rb.displayTree();
	}
	
	public class Node{
		private Node parentNode;
		private Node leftNode, rightNode;
		private Integer value;
		private boolean red = true;
		private boolean deleted = false;
		
		public Node(Integer value){
			this.value = value;
		}
		
		public Integer getValue(){
			return value;
		}
		
		public boolean isRed(){
			return red;
		}
		
		public void setRed(boolean b){
			red = b;
		}
		
		public boolean isDeleted(){
			return deleted;
		}
		
		public void setDeleted(boolean b){
			deleted = b;
		}
		
		public boolean isLeaf(){
			return ((leftNode == null) && (rightNode == null));
		}
		
		public void setLeft(Node node){
			if (leftNode == null){
				bondLeft(node);
				if (this.isRed()){
					Node parent = getParent();
					if (parent.getLeft() == this){
						parent.switchColor();
						switchColor();
						rotate();
					} else {
						parent.switchColor();
						node.switchColor();
						node.rotate();
						node.rotate();
					}
				}
			} else {
				verifyColors();
				leftNode.append(node);
			}
		}
		
		private void verifyColors() {
			if (! isRed() && getLeft() != null && getRight() != null)
				if ((getLeft().isRed()) && (getRight().isRed()))
					flipColor();
			if (getParent() == null) //is root node
				setRed(false);
			else if (isRed() && getParent().isRed()){
				Node grand = getParent().getParent();
				if (getParent().getLeft() == this){
					if (grand.getLeft() == getParent()){
						grand.switchColor();
						getParent().switchColor();
						getParent().rotate();
					} else {
						grand.switchColor();
						switchColor();
						rotate();
						rotate();
					}
				} else {
					if (grand.getRight() == getParent()){
						grand.switchColor();
						getParent().switchColor();
						getParent().rotate();
					} else {
						grand.switchColor();
						switchColor();
						rotate();
						rotate();
					}
				}
			}
			
		}

		public void setRight(Node node){
			if (rightNode == null){
				bondRight(node);
				if (this.isRed()){
					Node parent = getParent();
					if (parent.getRight() == this){
						parent.switchColor();
						switchColor();
						rotate();
					} else {
						parent.switchColor();
						node.switchColor();
						node.rotate();
						node.rotate();
					}
				}
			} else {
				verifyColors();
				rightNode.append(node);
			}
		}
		
		public Node getLeft(){
			return leftNode;
		}
		
		public Node getRight(){
			return rightNode;
		}
		
		public void bondLeft(Node node){
			leftNode = node;
			if (leftNode != null)
				leftNode.setParent(this);
		}
		
		public void bondRight(Node node){
			rightNode = node;
			if (rightNode != null)
				rightNode.setParent(this);
		}
		
		public void setParent(Node node){
			parentNode = node;
		}
		
		public Node getParent(){
			return parentNode;
		}
		
		public void append(Node node){
			int comp = getValue().compareTo(node.getValue());
			if (comp <= 0)
				setRight(node);
			else
				setLeft(node);
		}
		
		public void rotate(){
			Node parent = getParent();
			Node grand = parent.getParent();
			if (grand != null){
				if (grand.getLeft() == parent){
					grand.bondLeft(this);
				} else {
					grand.bondRight(this);
				}
			} else { //parent is root initially
				setParent(null);
			}
			if (parent.getLeft() == this){
				parent.bondLeft(getRight());
				bondRight(parent);
			} else {
				parent.bondRight(getLeft());
				bondLeft(parent);
			}
			verifyColors();
		}
		
		public void flipColor(){
			setRed(! isRed());
			getLeft().setRed(! getLeft().isRed());
			getRight().setRed(! getRight().isRed());
		}
		
		public void switchColor(){
			setRed(! isRed());
		}
	}

}
