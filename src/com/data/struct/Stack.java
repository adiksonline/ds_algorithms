package com.data.struct;

public class Stack<T> {
	T[] array;
	int size, maxSize;
	@SuppressWarnings("unchecked")
	public Stack(int maxSize){
		this.maxSize = maxSize;
		array = (T[]) new Object[maxSize];
	}
	
	public void push(T n){
		array[size++] = n;
	}
	
	public T pop(){
		return array[--size];
	}
	
	public T peek(){
		return array[size - 1];
	}
	
	public boolean isEmpty(){
		return (size == 0);
	}
	
	public boolean isFull(){
		return (size == maxSize);
	}
	
	public int getSize(){
		return size;
	}
	
	public String toString(){
		String s = "";
		for (int i = 0; i < size; i++)
			s += array[i];
		return s;
	}

}
