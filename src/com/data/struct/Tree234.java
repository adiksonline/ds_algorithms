package com.data.struct;

public class Tree234 {
	private Node rootNode;
	
	public Tree234(){
		rootNode = new Node();
	}
	
	public void insert(Integer key){
		//System.out.println("Inserting " + key);
		Node node = rootNode;
		while ((!node.isLeaf()) || (node.isFull())){
			if (node.isFull())
				node = breakNode(node);
			int i = 0;
			while ((i < 3) && (node.getItems()[i] != null)){
				//System.out.println("this is bad");
				if (node.getItems()[i] < key)
					i++;
				else
					break;
			}
			node = node.getChild(i);
		}
		node.insertItem(key);
	}
	
	public Node breakNode(Node node){
		Node coNode = new Node();
		coNode.insertItem(node.popItem(2));
		coNode.setChild(0, node.popChild(2));
		coNode.setChild(1, node.popChild(3));
		if (node == rootNode){
			Node newRoot = new Node();
			newRoot.insertItem(node.popItem(1));
			newRoot.setChild(0, node);
			newRoot.setChild(1, coNode);
			rootNode = newRoot;
			return rootNode;
		} else {
			Node parent = node.getParent();
			Integer key = node.popItem(1);
			parent.insertItem(key);
			int index;
			for (index = 0; index < 3; index++)
				if (parent.getChild(index) == node)
					break;
			parent.setChild(index++, node);
			parent.setChild(index, coNode);
			return parent;
		}
	}
	
	public Node search(Integer key){
		Node node = rootNode;
		while ((node != null) && (!node.hasItem(key))){
			int i = 0;
			while ((i < 3) && node.getItems()[i] != null){
				if (node.getItems()[i].compareTo(key) < 0)
					i++;
				else
					break;
			}
			node = node.getChild(i);
		}
		return node;
	}
	
	public void displayTree(){
		int space = 256;
		Stack<Node> globalStack = new Stack<Node>(1000);
		Stack<Node> localStack = new Stack<Node>(1000);
		globalStack.push(rootNode);
		boolean doNext = true;
		int count = 0;
		while (doNext ==true){
			if (count == 2)
				doNext = false;
			for (int i = 0; i < (space / 2 - 8); i++)
				System.out.print(" ");
			while (! globalStack.isEmpty()){
				Node item = globalStack.pop();
				if (item == null){
					for (int i = 0; i < 4; i++)
						localStack.push(null);
					System.out.print("__-__-__");
				} else {
					for (int i = 0; i < 4; i++)
						localStack.push(item.getChild(i));
					System.out.printf("%2d-%2d-%2d",
							item.getItems()[0], item.getItems()[1], item.getItems()[2]);
				}
				for (int i = 0; i < (space - 32) / 5; i++)
					System.out.print(" ");
			}
			while (! localStack.isEmpty())
				globalStack.push(localStack.pop());
			System.out.println();
			System.out.println();
			space /= 2;
			count++;
		}
		System.out.println();
		
	}
	
	public static void main(String[] args) {
		Tree234 tree = new Tree234();
		/*int[] n = {50, 40, 60, 30, 70};
		for (int i : n)
			tree.insert(i);*/
		for (int i = 30; i >= 1; i--){
			int n = (int) (Math.random() * 1000);
			tree.insert(n);
		}
		tree.displayTree();
		if (tree.search(30) != null)
			System.out.println("The node was found successfully");
		else
			System.out.println("Couldnt find the specified key");
	}
	
	public class Node{
		private Integer[] items;
		private Node[] children;
		private Node parent;
		
		public Node(){
			items = new Integer[3];
			children = new Node[4];
		}
		
		public Integer[] getItems(){
			return items;
		}
		
		public boolean hasItem(Integer item){
			for (int i = 0; i < 3; i++){
				if (items[i] == null)
					return false;
				else if (items[i].compareTo(item) == 0)
					return true;
			}
			return false;
		}
		
		public void insertItem(Integer item){
			if (isEmpty()){
				items[0] = item;
			} else {
				int i = 0;
				while ((items[i] != null) && (items[i].compareTo(item) < 0))
					i++;
				for (int j = 2; j > i; j--){
					items[j] = items[j - 1];
					children[j + 1]	= children[j];
				}
				items[i] = item;
			}
		}
		
		public void setChild(int pos, Node node){
			children[pos] = node;
			if (node != null)
				node.setParent(this);
		}
		
		public Node getChild(int pos){
			return children[pos];
		}
		
		public void setParent(Node parent){
			this.parent = parent;
		}
		
		public Node getParent(){
			return parent;
		}
		
		public Node popChild(int pos){
			Node node = children[pos];
			children[pos] = null;
			return node;
		}
		
		public Integer popItem(int pos){
			Integer item = items[pos];
			items[pos] = null;
			return item;
		}
		
		public boolean isLeaf(){
			for (int i = 0; i < 4; i++){
				if (children[i] != null)
					return false;
			}
			return true;
		}
		
		public boolean isEmpty(){
			for (int i = 0; i < 3; i++){
				if (items[i] != null)
					return false;
			}
			return true;
		}
		
		public boolean isFull(){
			return (items[2] != null);
		}
	}

}
