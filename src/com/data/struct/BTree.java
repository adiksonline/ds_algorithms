package com.data.struct;


public class BTree {
	private Node rootNode;
	private int ORDER;
	
	public BTree(int order){
		ORDER = order;
		rootNode = new Node(ORDER);
	}
	
	public void insert(Integer item){
		Node node = rootNode;
		while (!node.isLeaf()){
			int i = 0;
			while ((i < ORDER) && (node.getItems()[i] != null)){
				if (node.getItems()[i] < item)
					i++;
				else
					break;
			}
			node = node.getChild(i);
		}
		insertTo(node, item);
		
	}
	
	private void insertTo(Node node, Integer item){
		if (! node.isFull()){
			node.insertItem(item);
		} else {
			node.insertItem(item);
			Node coNode = new Node(ORDER);
			int mid = ORDER / 2;
			int i;
			for (i = mid + 1; i <= ORDER; i++){
				coNode.insertItem(node.popItem(i));
				coNode.setChild(i - (mid + 1), node.popChild(i));
			}
			coNode.setChild(i - (mid + 1), node.popChild(i));
			Node parent = node.getParent();
			if (parent == null){
				parent = new Node(ORDER);
				parent.setChild(0, node);
				rootNode = parent;
			}
			insertTo(parent, node.popItem(mid));
			node.getParent().setChild(node.getPosition() + 1, coNode);
		}
		
	}
	
	public Node search(Integer key){
		Node node = rootNode;
		while ((node != null) && (!node.hasItem(key))){
			int i = 0;
			while ((i < ORDER) && node.getItems()[i] != null){
				if (node.getItems()[i].compareTo(key) < 0)
					i++;
				else
					break;
			}
			node = node.getChild(i);
		}
		return node;
	}
	
	public void displayTree(){
		int len = ORDER * 2 + ORDER - 1;
		int space = 256;
		Stack<Node> globalStack = new Stack<Node>(1000);
		Stack<Node> localStack = new Stack<Node>(1000);
		globalStack.push(rootNode);
		boolean doNext = true;
		int count = 0;
		while (doNext ==true){
			if (count == 2)
				doNext = false;
			for (int i = 0; i < (space / 2 - len); i++)
				System.out.print(" ");
			while (! globalStack.isEmpty()){
				Node item = globalStack.pop();
				if (item == null){
					for (int i = 0; i <= ORDER; i++)
						localStack.push(null);
					System.out.print("__-__-__");
				} else {
					for (int i = 0; i <= ORDER; i++)
						localStack.push(item.getChild(i));
					Integer[] items = item.getItems();
					for (int i = 0; i < items.length; i++){
						if (i == items.length - 1){
							if (items[i] == null)
								System.out.print("__");
							else
								System.out.printf("%2d", items[i]);
						} else{
							if (items[i] == null)
								System.out.print("__-");
							else
								System.out.printf("%2d-", items[i]);
						}
					}
				}
				for (int i = 0; i < (space - len * (ORDER + 1)) / (ORDER + 2); i++)
					System.out.print(" ");
			}
			while (! localStack.isEmpty())
				globalStack.push(localStack.pop());
			System.out.println();
			System.out.println();
			space /= 2;
			count++;
		}
		System.out.println();
		
	}
	
	public static void main(String[] args) {
		BTree tree = new BTree(4);
		/*int[] n = {50, 15, 20, 30, 45, 25, 60, 80, 70, 35, 10, 75, 65, 55, 40, 90, 85, 82, 95};
		for (int i : n)
			tree.insert(i);*/
		for (int i = 30; i >= 1; i--){
			int n = (int) (Math.random() * 100);
			tree.insert(n);
		}
		tree.displayTree();
		if (tree.search(83) != null)
			System.out.println("The node was found successfully");
		else
			System.out.println("Couldnt find the specified key");

	}
	
	public class Node{
		private Integer[] items;
		private Node[] children;
		private Node parent;
		private int ORDER;
		private int itemCount;
		private int position;
		
		public Node(int order){
			ORDER = order;
			items = new Integer[ORDER + 1];
			children = new Node[ORDER + 2];
		}

		public Integer[] getItems(){
			Integer[] item = new Integer[ORDER];
			for (int i = 0; i < ORDER; i++)
				item[i] = items[i];
			return item;
		}
		
		public boolean hasItem(Integer item){
			for (int i = 0; i < ORDER; i++){
				if (items[i] == null)
					return false;
				else if (items[i].compareTo(item) == 0)
					return true;
			}
			return false;
		}
		
		public void insertItem(Integer item){
			if (isEmpty()){
				items[0] = item;
			} else {
				int i;
				for (i = itemCount; i > 0 && items[i - 1].compareTo(item) > 0; i--){
					items[i] = items[i - 1];
					setChild(i + 1, children[i]);
				}
				setChild(i + 1, null);
				items[i] = item;
			}
			itemCount++;
		}
		
		public void setChild(int pos, Node node){
			children[pos] = node;
			if (node != null){
				node.setParent(this);
				node.setPosition(pos);
			}
		}
		
		private void setPosition(int pos) {
			position = pos;
		}
		
		private int getPosition(){
			return position;
		}

		public Node getChild(int pos){
			return children[pos];
		}
		
		public void setParent(Node parent){
			this.parent = parent;
		}
		
		public Node getParent(){
			return parent;
		}
		
		public Node popChild(int pos){
			Node node = children[pos];
			children[pos] = null;
			return node;
		}
		
		public Integer popItem(int pos){
			Integer item = items[pos];
			items[pos] = null;
			itemCount--;
			return item;
		}
		
		public boolean isLeaf(){
			for (int i = 0; i <= ORDER; i++){
				if (children[i] != null)
					return false;
			}
			return true;
		}
		
		public boolean isEmpty(){
			for (int i = 0; i < ORDER; i++){
				if (items[i] != null)
					return false;
			}
			return true;
		}
		
		public boolean isFull(){
			return (items[ORDER - 1] != null);
		}
		
	}

}
