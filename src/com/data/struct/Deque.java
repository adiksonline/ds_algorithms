package com.data.struct;

public class Deque {
	private int maxSize;
	private int front = 0, rear = 0;
	private int[] ar;
	
	public Deque(int size){
		maxSize = size + 1;
		ar = new int[maxSize];
	}
	
	public void insertRight(int num){
		if (isFull())
			throw new ArrayIndexOutOfBoundsException("Deque is full. Cant insert " + num);
		if (front == maxSize)
			front = 0;
		ar[front++] = num;
	}
	
	public void insertLeft(int num){
		if (isFull())
			throw new ArrayIndexOutOfBoundsException("Deque is full. Cant insert " + num);
		if (rear == 0)
			rear = maxSize;
		ar[--rear] = num;
	}
	
	public int removeRight(){
		if (front == 0)
			front = maxSize;
		return ar[--front];
	}
	
	public int removeLeft(){
		if (rear == maxSize)
			rear = 0;
		return ar[rear++];
	}
	
	public boolean isFull(){
		if ((front - rear == maxSize - 1) || (front + 1 == rear))
			return true;
		return false;
	}
	
	public boolean isEmpty(){
		if ((rear - front == maxSize) || (rear == front))
			return true;
		return false;
	}
	
	public int size(){
		if (front >= rear)
			return front - rear;
		else
			return maxSize - (rear - front);
	}

}
