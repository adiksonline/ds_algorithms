package com.data.struct;

public class LeakingStack{
	private StackNode firstNode, lastNode;
	private int size;
	private int maxSize;
	
	public LeakingStack(int maxSize){
		this.maxSize = maxSize;
	}
	
	public void push(Node<?> node){
		if (size == maxSize){
			lastNode = lastNode.getPrevious();
			lastNode.setNext(null);
			size--;
		}
		StackNode stackNode = new StackNode(node);
		if (firstNode == null){
			lastNode = stackNode;
			lastNode.setPrevious(firstNode);
		} else {
			firstNode.setPrevious(stackNode);
		}
		stackNode.setNext(firstNode);
		firstNode = stackNode;
		size++;
	}
	
	public Node<?> pop(){
		Node<?> node = firstNode.getNode();
		firstNode = firstNode.getNext();
		firstNode.setPrevious(null);
		size--;
		return node;
	}
	
	public Node<?> peek(){
		return firstNode.getNode();
	}
	
	public int getSize(){
		return size;
	}
	
	public boolean isEmpty(){
		return (size == 0);
	}
	
	public String toString(){
		String s = "";
		StackNode snode = lastNode;
		while (snode != null){
			s += snode.getNode().getData();
			snode = snode.getPrevious();
		}
		return s;
	}
	
	public class Node <T>{
		private T data;
		
		public Node(T data){
			this.data = data;
		}
		
		public T getData(){
			return data;
		}
	}
	
	public class StackNode{
		private StackNode nextNode, previousNode;
		private Node<?> node;
		
		public StackNode(Node<?> node){
			this.node = node;
		}
		
		public Node<?> getNode(){
			return node;
		}
		
		public StackNode getNext(){
			return nextNode;
		}
		
		public void setNext(StackNode stackNode){
			nextNode = stackNode;
		}
		
		public StackNode getPrevious(){
			return previousNode;
		}
		
		public void setPrevious(StackNode stackNode){
			previousNode = stackNode;
		}
		
	}
}