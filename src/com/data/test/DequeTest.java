package com.data.test;

import com.data.struct.Deque;

public class DequeTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque d = new Deque(5);
		int temp;
		while (! d.isFull()){
			temp = (int) (Math.random() * 100);
			System.out.print(temp + " ");
			d.insertRight(temp);
		}
		/*d.insertLeft(12);
		d.insertRight(15);
		d.insertRight(20);
		d.insertRight(11);
		d.insertLeft(4);
		d.removeRight();
		d.insertLeft(43);
	    d.insertLeft(3);
	    d.insertLeft(90);*/
		System.out.println(d.isFull());
		while (! d.isEmpty()){
			System.out.print(d.removeRight() + " ");
		}
		System.out.println();

	}

}
