package com.data.apply;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Knapsack {
	public static StackX stack = new StackX(10);
	
	public static void getCombi(int[] array, int target){
		for (int i = 0; i < array.length; i++){
			if (array[i] + stack.getSum() > target){
				continue;
			} else if (array[i] + stack.getSum() == target){
				stack.push(array[i]);
				if (stack.getSize() == 1)
				    stack.displayStack();
				return;
			} else {
				stack.push(array[i]);
				//stack.displayStack();
				getCombi(Arrays.copyOfRange(array, i + 1, array.length), target);
			}
			if (stack.getSum() != target){
				stack.pop();
				//stack.displayStack();
			} else {
				stack.displayStack();
				break;
			}
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		System.out.print("Enter space separated numbers: ");
		String s = getString();
		System.out.print("Enter the target: ");
		int target = getInt();
		int[] ar = toIntArray(s.split(" "));
		getCombi(ar, target);

	}
	
	public static String getString() throws IOException{

		InputStreamReader in = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(in);
		String s = br.readLine();
		return s;
	}
	
	public static int getInt() throws NumberFormatException, IOException{
		return Integer.parseInt(getString());
	}
	
	public static int[] toIntArray(String[] ar){
		int[] array = new int[ar.length];
		for (int i = 0; i < ar.length; i ++	)
			array[i] = Integer.parseInt(ar[i]);
		return array;
	}
	
	public static class StackX{
		private int[] array;
		private int size;
		private int top = -1;
		
		public StackX(int size){
			this.size = size;
			array = new int[size];
		}
		
		public void push(int num){
			array[++top] = num;
		}
		
		public int pop(){
			return array[top--];
		}
		
		public int peek(){
			return array[top];
		}
		
		public boolean isEmpty(){
			return (top == -1);
		}
		
		public boolean isFull(){
			return (top == size - 1);
		}
		
		public int getSize(){
			return top + 1;
		}
		
		public int getSum(){
			if (isEmpty())
				return 0;
			else{
				int sum = 0;
				for (int i = 0; i <= top; i++)
					sum += array[i];
				return sum;
			}
		}
		
		public void displayStack(){
			String s = "{ ";
			for (int i = 0; i <= top; i++)
				s += array[i] + " ";
			s += "}";
			System.out.println(s);
		}
	}

}
