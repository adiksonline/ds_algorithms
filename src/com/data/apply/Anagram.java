package com.data.apply;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Anagram {
	private char[] ar;
	private int size;
	private int count;
	
	public Anagram(char[] ar){
		this.ar = ar;
		size = ar.length;
	}
	
	public void anagramize(){
		doAnagram(ar.length);
	}
	
	public void doAnagram(int len){
		if (len == 1)
			return;
		for (int i = 0; i < len; i++){
			doAnagram(len - 1);
			if (len == 2)
				display();
			rotate(len);
		}
	}
	
	private void rotate(int len) {
		// TODO Auto-generated method stub
		len = size - len;
		char temp = ar[len];
		for (int i = len; i < size - 1; i++)
			ar[i] = ar[i + 1];
		ar[size - 1] = temp;
		
	}

	private void display() {
		// TODO Auto-generated method stub
		if (count <  99)
			System.out.print(" ");
		if (count <  9)
			System.out.print(" ");
		String s = "";
		for (char ch : ar)
			s += ch;
		System.out.print(++count + ". " + s + " ");
		if (count % 6 == 0)
			System.out.println();
		
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.print("Input word to get anagram: ");
		String s = getString();
		char[] array = new char[s.length()];
		for (int i = 0; i < s.length(); i++)
			array[i] = s.charAt(i);
		Anagram anagram = new Anagram(array);
		anagram.anagramize();

	}
	
	public static String getString() throws IOException{

		InputStreamReader in = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(in);
		String s = br.readLine();
		return s;
	}

}
