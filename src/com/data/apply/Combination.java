package com.data.apply;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Combination {
	private static StackX stack = new StackX(1000);
	private static String[] ar;
	
	public static void doCombi(int n, int r){
		//System.out.printf("%d %d ", n, r);
		for (int i = 0;i < n; i++){
			if (n - i < r || r == 0)
				return;
			stack.push(ar[ar.length - n + i]);
			if (r == 1){
				stack.displayString();
				stack.pop();
				continue;
			} else {
				doCombi(n - i - 1, r - 1);
				stack.pop();
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.print("Enter the team codes seperated by spaces: ");
		String s = getString();
		ar = s.split(" ");
		System.out.print("Enter the group count: ");
		int count = getInt();
		doCombi(ar.length, count);

	}
	
	public static String getString() throws IOException{

		InputStreamReader in = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(in);
		String s = br.readLine();
		return s;
	}
	
	public static int getInt() throws NumberFormatException, IOException{
		return Integer.parseInt(getString());
	}
	
	public static class StackX{
		private String[] array;
		private int size;
		private int top = -1;
		
		public StackX(int size){
			this.size = size;
			array = new String[size];
		}
		
		public void push(String val){
			array[++top] = val;
		}
		
		public String pop(){
			return array[top--];
		}
		
		public String peek(){
			return array[top];
		}
		
		public boolean isEmpty(){
			return (top == -1);
		}
		
		public boolean isFull(){
			return (top == size - 1);
		}
		
		public int getSize(){
			return top + 1;
		}
		
		public void displayString(){
			String s = "";
			for (int i = 0; i <= top; i++)
				s += array[i];
			System.out.println(s);
		}
	}

}
