package com.data.apply;

public class Power {

	/**
	 * @param args
	 */
	public static int getPower(int a, int b){
		if (b == 1)
			return a;
		int x = a * a;
		int y = b / 2;
		if (b % 2 == 0)
			return getPower(x, y);
		else
			return getPower(x, y) * a;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getPower(3, 18));

	}

}
