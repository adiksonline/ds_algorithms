package com.data.apply;

public class Hanoi {
	
	public void move(int n, String from, String to, String temp){
		if (n == 1){
			System.out.printf("%s -> %s\n", from, to);
		} else {
			move(n -1, from, temp, to);
			System.out.printf("%s -> %s\n", from, to);
			move(n - 1, temp, to, from);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hanoi hanoi = new Hanoi();
		hanoi.move(3, "A", "C", "B");

	}

}
