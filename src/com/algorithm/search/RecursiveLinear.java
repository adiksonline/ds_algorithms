package com.algorithm.search;

public class RecursiveLinear {
	private int[] lst;
	private int val;
	
	public RecursiveLinear(int[] lst, int val){
		this.lst = lst;
		this.val = val;
	}
	
	public int search(){
		//toString();
		return search(0);
	}
	public int search(int v){
		System.out.println(sub(v));
		if (v >= lst.length - 1)
			return -1;
		if (lst[v] == val)
			return v;
		int l = search(++v);
		return l;
	}
	
	public String sub(int v){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < v; i++)
			sb.append("   ");
		sb.append("{ ");
		for (int i = v; i < lst.length; i++)
			sb.append(lst[i] + " ");
		sb.append("}");
		return sb.toString();
		
	}
	
	public String toString(){
		return sub(0);
	}

}
