/**
 * 
 */
package com.algorithm.search;

import java.util.Arrays;

/**
 * @author ADIKSONLINE
 *
 */
public class BinarySearch {
	private int[] lst;
	private int mid;
	
	public BinarySearch(int[] lst){
		this.lst = lst;
		Arrays.sort(this.lst);
	}
	public int search(int num) throws ArithmeticException{
		int low = 0, high = lst.length - 1;
		mid = (low + high) / 2;
		while (num != lst[mid]){
			if ((high - low <= 0) && num != mid)
				break;
			
			if (num > lst[mid])
				low = mid + 1;
			else if (num < lst[mid])
				high = mid;
			mid = (low + high) / 2;
		}
		if (num == lst[mid])
			return mid;
		else
			throw new ArithmeticException(num + " not in the list!");
	}
	public String toString(){
		StringBuilder st = new StringBuilder();
		st.append("{ ");
		for (int elem : lst)
			st.append(elem + " ");
		st.append("}");
		return st.toString();
	}

}
