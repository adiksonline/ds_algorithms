package com.algorithm.test;

import java.util.Random;

import com.algorithm.sort.BiBubbleSort;

public class BiBubbleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random gen = new Random();
		int[] lst = new int[10];
		for (int i = 0; i < lst.length; i++){
			lst[i] = 11 + gen.nextInt(89);
		}
		BiBubbleSort merge = new BiBubbleSort(lst);
		System.out.println("Before:");
		System.out.println(merge);
		System.out.println();
		merge.sort();
		System.out.println("After:");
		System.out.println(merge);

	}

}
