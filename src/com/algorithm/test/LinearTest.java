package com.algorithm.test;

import java.util.Random;

import com.algorithm.search.RecursiveLinear;

public class LinearTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random gen = new Random();
		int[] lst = new int[50];
		for (int i = 0; i < lst.length; i++){
			lst[i] = 11 + gen.nextInt(40);
		}
		int x = lst[gen.nextInt(lst.length)] + 1;
		RecursiveLinear rl = new RecursiveLinear(lst, x);
		System.out.printf("%d was found at %d", x, rl.search());

	}

}
