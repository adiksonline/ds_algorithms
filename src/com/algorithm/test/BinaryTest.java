/**
 * 
 */
package com.algorithm.test;

import java.util.Random;

import com.algorithm.search.BinarySearch;

/**
 * @author ADIKSONLINE
 *
 */
public class BinaryTest {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Random gen = new Random();
		int[] lst = new int[50];
		for (int i = 0; i < lst.length; i++){
			lst[i] = 11 + gen.nextInt(40);
		}
		BinarySearch search = new BinarySearch(lst);
		int rand = 11 + gen.nextInt(40);
		System.out.println(search);
		System.out.printf("Number %d was found at index %d\n", rand, search.search(rand));

	}

}
