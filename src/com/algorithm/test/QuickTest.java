/**
 * 
 */
package com.algorithm.test;

import java.util.Random;

import com.algorithm.sort.QuickSort;

/**
 * @author ADIKSONLINE
 *
 */
public class QuickTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random gen = new Random();
		int[] lst = new int[30];
		for (int i = 0; i < lst.length; i++){
			lst[i] = 11 + gen.nextInt(89);
		}
		QuickSort merge = new QuickSort(lst);
		System.out.printf("%-10s", "Unsorted: ");
		System.out.println(merge);
		merge.sort();
		System.out.printf("%-10s", "Sorted: ");
		System.out.println(merge);

	}

}
