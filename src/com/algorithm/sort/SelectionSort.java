/**
 * 
 */
package com.algorithm.sort;

/**
 * @author ADIKSONLINE
 *
 */
public class SelectionSort {
	private int[] lst;
	
	public SelectionSort(int[] lst){
		this.lst = lst;
	}
	public void sort(){
		for (int i = 0; i < lst.length - 1; i++){
			int min = i;
			int minx = i;
			for (int j = i + 1; j < lst.length; j++){
				if (lst[j] < lst[min])
					min = j;
			}
			int t = lst[minx];
			lst[minx] = lst[min];
			lst[min] = t;
		}
	}
	public String toString(){
		StringBuilder st = new StringBuilder();
		st.append("{ ");
		for (int elem : lst)
			st.append(elem + " ");
		st.append("}");
		return st.toString();
	}

}
