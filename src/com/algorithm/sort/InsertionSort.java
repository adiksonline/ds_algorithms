/**
 * 
 */
package com.algorithm.sort;

/**
 * @author ADIKSONLINE
 *
 */
public class InsertionSort {
	private int[] lst;
	
	public InsertionSort(int[] lst){
		this.lst = lst;
	}
	
	public void sort(){
		/*
		This is the bubble sort algorithm:
		int i, j;
		for (i = lst.length - 1; i > 1; i--)
			for (j = 0; j < i; j ++){
				if (lst[j] > lst[j + 1])
					swap(j, j + 1);*/
		for (int i = 1; i < lst.length; i++){
			int t = lst[i];
			int j;
			for (j = i - 1; j >= 0 && t < lst[j]; j--)
	            lst[j + 1] = lst[j];
		    lst[j + 1] = t;
			/*int j = i -1;
			while (lst[j] > t){
				lst[j + 1] = lst[j];
				j--;
			}
			lst[j] = t;*/
		}
	}
	
	public String toString(){
		StringBuilder st = new StringBuilder();
		st.append("{ ");
		for (int elem : lst)
			st.append(elem + " ");
		st.append("}");
		return st.toString();
	}

}
