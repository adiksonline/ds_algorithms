/**
 * 
 */
package com.algorithm.sort;

/**
 * @author adiksonline
 *
 */
public class MergeSort {
	
	private int[] lst;
	private int[] combi;

	public MergeSort(int[] lst){
		this.lst = lst;
		combi = new int[lst.length];
	}
	
	public void sort(){
		System.out.println(subarray(0, lst.length));
		sortArray(0, lst.length - 1);
		System.out.println(subarray(0, lst.length));
	}

	private void sortArray(int low, int high) {
		// TODO Auto-generated method stub
		if (low >= high)
			return;
	    int midl = (low + high) / 2;
	    int midr = midl + 1;
	    sortArray(low, midl);
	    sortArray(midr, high);
		mergeArray(low, midl, midr, high);
	}

	private void mergeArray(int low, int midl, int midr, int high) {
		// TODO Auto-generated method stub
		int left = low;
		int right = midr;
		int i = low;
		while ((left <= midl) && (right <= high)){
			if (lst[left] <= lst[right])
				combi[i++] = lst[left++];
			else
				combi[i++] = lst[right++];
		}
		while (left <= midl)
			combi[i++] = lst[left++];
		while (right <= high)
			combi[i++] = lst[right++];
		for (int j = low; j <= high; j++)
			lst[j] = combi[j];
	}
	
	public String subarray(int low, int high){
		String st = "{ ";
		for (int i = low; i < high; i++){
			st += lst[i] + " ";
		}
		st += "}";
		return st;
	}

}
