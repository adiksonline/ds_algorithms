package com.algorithm.sort;

public class BiBubbleSort {
	private int[] lst;
	private int sw;
	
	public BiBubbleSort(int[] lst){
		this.lst = lst;
	}
	
	public void sort(){
		int j, i, count = 0;
		for (i = lst.length - 1; i > 1; i--){
			for (j = 0; j < i; j++){
				if (lst[j] > lst[j + 1]){
					swap(j, j + 1);
					sw++;
				}
				if (lst[i - j] < lst[i - j - 1]){
					swap(i - j, i - j - 1);
					sw++;
				}
			}
			System.out.println(this);
			count++;
			if (count == lst.length / 2)
				break;
		}
	}
	
	public void swap(int ind, int dest){
		int temp = lst[ind];
		lst[ind] = lst[dest];
		lst[dest] = temp;
	}
	
	public String toString(){
		String st = "{ ";
		for (int elem : lst)
			st += elem + " ";
		st += "} Number of swaps:" + sw;
		return st;
	}

}
