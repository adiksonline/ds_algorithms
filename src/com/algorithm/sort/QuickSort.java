package com.algorithm.sort;

public class QuickSort {
	
	public int[] lst;
	
	public QuickSort(int[] lst){
		this.lst = lst;
	}
	
	public void sort(){
		/*
		 * I guess the insertion sort will be better off for smaller arrays
		 * than one big array, so its better to use the insertion sort for 
		 * the base cases of the quick sort than applying it after quick sort.
		 */
		qSort(0, lst.length - 1);
		//iSort(0, lst.length - 1); //uncomment this line for approach TWO and comment the one in method qSort();
	}
	
	public void qSort(int left, int right){
		if (right - left + 1 <= 3){
			iSort(left, right); //comment this line if iSort is not commented in method sort();
			return;
		} else{
			int pivot = manualSort(left, right);
			int partPoint = partition(left, right, pivot);
			qSort(left, partPoint - 1);
			qSort(partPoint + 1, right);
		}
	}
	
	public void iSort(int left, int right){
		for (int i = left + 1; i <= right; i++){
			int t = lst[i];
			int j;
			for (j = i - 1; j >= left && t < lst[j]; j--)
	            lst[j + 1] = lst[j];
		    lst[j + 1] = t;
		}
	}
	
	public int manualSort(int left, int right){
		int mid = (left + right) / 2;
		if (lst[left] > lst[mid])
			swap(left, mid);
		if (lst[left] > lst[right])
			swap(left, right);
		if (lst[mid] > lst[right])
			swap(mid, right);
		swap(mid, right);
		return lst[right];
	}
	
	private void swap(int left, int right) {
		// TODO Auto-generated method stub
		int temp = lst[left];
		lst[left] = lst[right];
		lst[right] = temp;
		
	}

	public int partition(int left, int right, int pivot){
		int leftPart = left;
		int rightPart = right;
		while (true){
			while (lst[++leftPart] < pivot);
			while (lst[--rightPart] > pivot);
			if (rightPart <= leftPart)
				break;
			else
				swap(leftPart, rightPart);
		}
		swap(leftPart, right);
		return leftPart;
	}
	
	public String toString(){
		String st = "";
		st += "{ ";
		for (int elem : lst)
			st += elem + " ";
		st += "}";
		return st;
	}

}
