package com.algorithm.sort;

public class ShellSort {
	private int[] lst;
	
	public ShellSort(int[] lst){
		this.lst = lst;
	}
	
	public void sort(){
		int h = 1;
		int high, testNum;
		int temp;
		while (h < lst.length / 3.0)
			h = h * 3 + 1;
		while (h >= 1){
			high = h;
			while (high < lst.length){
				testNum = high - h;
				temp = lst[high];
				while (testNum >= 0 && temp < lst[testNum]){
					lst[testNum + h] = lst[testNum];
					testNum -= h;
				}
				lst[testNum + h] = temp;
				high += h;
			}
			h = (h - 1) / 3;
		}
	}
	
	public String toString(){
		StringBuilder st = new StringBuilder();
		st.append("{ ");
		for (int elem : lst)
			st.append(elem + " ");
		st.append("}");
		return st.toString();
	}

}
